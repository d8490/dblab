USE gnz;

CREATE TABLE organisation (
  OrgID INT(11) NOT NULL AUTO_INCREMENT,
  Abbreviation VARCHAR(45) NOT NULL,
  Name VARCHAR(45) NOT NULL UNIQUE,  
  Street VARCHAR(45) NOT NULL,
  StrNumber INT NOT NULL,
  PostalCode INT NOT NULL,
  City VARCHAR(45) NOT NULL,
  Category ENUM('Com', 'Uni', 'Rec'),
  Budget INT NOT NULL DEFAULT 0,
  PRIMARY KEY (OrgID)
  )ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ------------------  
CREATE TABLE phone (
  OrgID int(11) NOT NULL,
  PhoneNumber char(10) NOT NULL,
  PRIMARY KEY (OrgID,`PhoneNumber`),
  CONSTRAINT phone_org FOREIGN KEY (OrgID) REFERENCES organisation (OrgID)
  ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

  
CREATE TABLE executive (
  ExecID INT NOT NULL AUTO_INCREMENT,
  Name VARCHAR(45) NOT NULL,
  Surname VARCHAR(45) NOT NULL,
  PRIMARY KEY (ExecID)
  );
  
CREATE TABLE program (
  ProgrID INT NOT NULL AUTO_INCREMENT,
  Name VARCHAR(45) NOT NULL,
  StartDate DATE NOT NULL,
  EndDate DATE NOT NULL,
  ELIDEK_Department TINYINT NOT NULL,
  PRIMARY KEY (ProgrID),
  CONSTRAINT chk_progrdate CHECK ((EndDate > StartDate)= TRUE)
  );

CREATE TABLE scientific_field (
  FieldID INT NOT NULL AUTO_INCREMENT,
  Name VARCHAR(45) NOT NULL,
  PRIMARY KEY (FieldID)
  );


CREATE TABLE researcher (
  ResearcherID int(11) NOT NULL AUTO_INCREMENT,
  Name varchar(45) NOT NULL,
  Surname varchar(45) NOT NULL,
  Sex char(1) NOT NULL DEFAULT 'O' COMMENT 'F\nM\nO',
  BirthDate date NOT NULL,
  OrgID INT(11) NOT NULL,
  Work_startdate date NOT NULL,
  PRIMARY KEY (ResearcherID),
  CONSTRAINT OrgID FOREIGN KEY (OrgID) REFERENCES organisation (OrgID)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT chk_gender CHECK (Sex='F' OR Sex='M' OR Sex='O'),
  CONSTRAINT chk_oldenough CHECK (DATEDIFF(Work_startdate, BirthDate)>=6570)
 ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
-- --------------------

CREATE TABLE `project`( 
ProjID int(11) NOT NULL AUTO_INCREMENT,
Title VARCHAR(45) NOT NULL,
Summary TEXT(5000) NOT NULL DEFAULT '', 
FundingAmount INT NOT NULL DEFAULT 0,
StartDate DATE NOT NULL ,
EndDate DATE NOT NULL ,
ScientificDirectorID INT NOT NULL ,
EvaluatorID INT NOT NULL,
EvalGrade CHAR(1) NOT NULL,
EvalDate DATE NOT NULL ,
OrgID INT NOT NULL ,
ExecID INT NOT NULL ,
ProgrID INT NOT NULL ,
PRIMARY KEY (ProjID),
CONSTRAINT pr_director FOREIGN KEY (ScientificDirectorID) REFERENCES researcher(ResearcherID)
  ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT pr_evaluator FOREIGN KEY (EvaluatorID) REFERENCES researcher(ResearcherID)
  ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT pr_org FOREIGN KEY (OrgID) REFERENCES organisation (OrgID)
  ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT pr_exec FOREIGN KEY (ExecID) REFERENCES executive(ExecID)
  ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT pr_prog FOREIGN KEY (ProgrID) REFERENCES program (ProgrID)
  ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT chk_funding CHECK (FundingAmount>=100000 AND FundingAmount<=1000000),
CONSTRAINT chk_grade CHECK (EvalGrade in ('A','B','C','D','F')),
CONSTRAINT chk_date CHECK ((EndDate > StartDate)= TRUE),
CONSTRAINT chk_years CHECK (DATEDIFF(EndDate, StartDate)>=365 AND DATEDIFF(EndDate, StartDate)<=1460)
)  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE works_at (
  ResearcherID int(11) NOT NULL,
  ProjID int(11) NOT NULL,
  StartDate DATE NOT NULL,
  PRIMARY KEY (ResearcherID,`ProjID`),
  CONSTRAINT work_proj FOREIGN KEY (ProjID) REFERENCES project(ProjID)
  ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT work_researcher FOREIGN KEY (ResearcherID) REFERENCES researcher(ResearcherID)
  ON DELETE CASCADE ON UPDATE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE refers_to (
  ProjID INT NOT NULL,
  FieldID INT NOT NULL,
  PRIMARY KEY (ProjID, FieldID),
CONSTRAINT refer_proj FOREIGN KEY (ProjID) REFERENCES project(ProjID)
  ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT refer_field FOREIGN KEY (FieldID) REFERENCES scientific_field(FieldID)
  ON DELETE CASCADE ON UPDATE CASCADE
  )ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
  

CREATE TABLE deliverable (
  DeliverID INT NOT NULL ,
  ProjID INT NOT NULL,
  Title VARCHAR(45) NOT NULL,
  Summary TEXT(2000) NOT NULL DEFAULT '',
  DeliveryDate DATE NOT NULL,
  PRIMARY KEY (ProjID, DeliverID),
  CONSTRAINT deliver_proj FOREIGN KEY (ProjID) REFERENCES project(ProjID)
  ON DELETE cascade ON UPDATE CASCADE
  );

CREATE TABLE company (
  CompID INT NOT NULL,
  Equity INT NOT NULL,
   PRIMARY KEY (CompID),
   CONSTRAINT company_org FOREIGN KEY (CompID) REFERENCES organisation (OrgID)
   ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE university (
  UniID INT NOT NULL,
  Ministry_Funds INT NOT NULL,
  PRIMARY KEY (UniID),
  CONSTRAINT uni_org FOREIGN KEY (UniID) REFERENCES organisation (OrgID)
  ON DELETE CASCADE ON UPDATE CASCADE
  );
  
CREATE TABLE research_center (
  RCID INT NOT NULL,
  Ministry_Private_Funds INT NOT NULL,
  PRIMARY KEY (RCID),
  CONSTRAINT r_cntr_org FOREIGN KEY (RCID) REFERENCES organisation (OrgID)
  ON DELETE CASCADE ON UPDATE CASCADE
  );
  
DELIMITER $$ 
CREATE TRIGGER org_type
after insert ON organisation
for each row
BEGIN
   IF (NEW.Category = 'Uni')
   THEN 
     INSERT INTO university (UniID , Ministry_Funds)
	 VALUES (NEW.OrgID, NEW.Budget);
END IF;
   IF (NEW.Category = 'Com')
     THEN 
      INSERT INTO company (CompID , Equity)
       VALUES (NEW.OrgID, NEW.Budget);
      
END IF;      
   IF (NEW.Category = 'Rec')
   THEN
INSERT INTO research_center (RCID , Ministry_Private_Funds)
 VALUES (NEW.OrgID, NEW.Budget);

END IF;
END$$
DELIMITER ;

DELIMITER $$ 
CREATE TRIGGER org_type_update
after update ON organisation
for each row
BEGIN
   IF (OLD.Category = 'Uni')
   THEN 
     UPDATE university SET Ministry_Funds = NEW.Budget
      WHERE UniID = old.OrgID;
END IF;
   IF (OLD.Category = 'Com')
     THEN 
      UPDATE company SET Equity = NEW.Budget   
	WHERE CompID = old.OrgID;
END IF;      
   IF (OLD.Category = 'Rec')
   THEN
UPDATE research_center SET Ministry_Private_Funds = NEW.Budget
WHERE RCID= old.OrgID;
END IF;
END$$
DELIMITER ;

DELIMITER $$ 
CREATE TRIGGER org_type_delete
after DELETE ON organisation
for each row
BEGIN
   IF (OLD.Category = 'Uni')
   THEN 
     DELETE FROM university 
      WHERE UniID = old.OrgID;
END IF;
   IF (OLD.Category = 'Com')
     THEN 
      DELETE FROM company 
      WHERE CompID = old.OrgID;
END IF;      
   IF (OLD.Category = 'Rec')
   THEN
     DELETE FROM research_center 
    WHERE RCID= old.OrgID;
END IF;
END$$
DELIMITER ;

drop view if exists res_proj;
CREATE VIEW res_proj as
SELECT r.ResearcherID as researcher, r.name as name, r.surname as surname, p.projID as proj, p.title as title
FROM researcher r
INNER JOIN works_at w ON (r.ResearcherID = w.ResearcherID)
INNER JOIN project p ON (p.projID=w.projID)
ORDER BY r.ResearcherID;

drop view if exists proj_field;
CREATE VIEW proj_field as 
SELECT p.projID as proj, p.title AS title, f.name as scientificfield
FROM project p
INNER JOIN refers_to ref ON (ref.projID = p.projID)
INNER JOIN scientific_field f ON (f.FieldID=ref.FieldID)
-- ORDER BY p.projID;
ORDER BY p.projID;

drop view if exists field_proj;
CREATE VIEW field_proj as 
SELECT f.name,f.FieldID as scientificfield, p.projID as proj, p.title AS title
FROM project p
INNER JOIN refers_to ref ON (ref.projID = p.projID)
INNER JOIN scientific_field f ON (f.FieldID=ref.FieldID)
ORDER BY scientificfield;

CREATE INDEX project_start_date ON project (StartDate);
CREATE INDEX project_end_date ON project (EndDate);
CREATE INDEX researcher_Birthdate ON researcher (BirthDate);
CREATE INDEX project_funding_amount ON project (FundingAmount);
CREATE INDEX work_ResearcherID ON works_at (ResearcherID);

drop view if exists yearly;
create view yearly as 
(SELECT o.Name, o.OrgID, year(p.StartDate) as Year, count(p.ProjID) as Projects
FROM  organisation as o 
INNER JOIN project as p USE INDEX (project_start_date) ON p.OrgID=o.OrgID

GROUP BY o.OrgID, year(p.StartDate)
HAVING Projects>=10);

drop view if exists biyearly;
create view biyearly as 
(SELECT o.Name, o.OrgID, CONCAT(o.year, '-', y.year) as Year, o.Projects  
FROM  yearly as o
inner join yearly as y on o.OrgID=y.OrgID
WHERE (o.Year=y.Year-1) AND (o.Projects=y.Projects) 
GROUP BY o.OrgID, o.Year
);


