-- Drop Views

DROP VIEW if exists res_proj;
DROP VIEW if exists proj_field;
DROP VIEW if exists field_proj;
DROP VIEW if exists yearly;
DROP VIEW if exists biyearly;

-- Drop Tables

DROP TABLE deliverable;
DROP TABLE works_at;
DROP TABLE refers_to;
DROP TABLE company;
DROP TABLE university;
DROP TABLE research_center;
DROP TABLE project;
DROP TABLE researcher;
DROP TABLE phone;
DROP TABLE organisation;
DROP TABLE executive;
DROP TABLE program;
DROP TABLE scientific_field;

-- DROP DATABASE GNZ;
