<!DOCTYPE html>
<html>
<head>

<?php include 'style.php';?>

<title> E.L.I.D.E.K. </title>
</head>

<body>
<h1>E.L.I.D.E.K.</h1>



<a href = "<?php echo "queries/query3.1a.php";?>" ><button class="button button1" >Show Available Programs</button></a> <br>
<a href = "<?php echo "queries/query3.1b.php";?>" ><button class="button button1" >Show Available Projects</button></a> <br>
<a href = "<?php echo "queries/query3.2a.php";?>" ><button class="button button1" >Projects per Researchers</button></a><br>
<a href = "<?php echo "queries/query3.2.b.php";?>" ><button class="button button1" >Scientific Fields of Projects</button></a> <br>
<a href = "<?php echo "queries/query3.3.php";?>" ><button class="button button1" >Info about Scientific Fields</button></a><br>
<a href = "<?php echo "queries/query3.4.php";?>" ><button class="button button1" >Organisations with the same number of projects within two consecutive years</button></a><br>
<a href = "<?php echo "queries/query3.5.php";?>" ><button class="button button1" >Top 3 Pairs of Scientific Fields</button></a> <br>
<a href = "<?php echo "queries/query3.6.php";?>" ><button class="button button1" >Young Researchers working on Active Projects</button></a><br>
<a href = "<?php echo "queries/query3.7.php";?>" ><button class="button button1" >Top 5 Executives that have funded a Specific Company</button></a><br>
<a href = "<?php echo "queries/query3.8.php";?>" ><button class="button button1" >Researchers that work on Projects With no Deliverables</button></a> <br>
<br> <br>

<a href = "<?php echo "insert/insert.php";?>" ><button class="button button2" >Insert New Data</button></a>
<a href = "<?php echo "update/update.php";?>" ><button class="button button2" >Manipulate Existing Data (Update or Delete)</button></a>
<a href = "<?php echo "index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>