<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>
<body>

<h1>Researcher Working on Project</h1>
<h2>Be mindful! A researcher can only work on projects of their associated organisation!</h2>
<?php
$sql = "SELECT ResearcherID, Name, Surname, OrgID from researcher";
$result = $conn->query($sql);
?>

<form method="post" action="INS/insert_worksat.php">

<p>
    <label for="person">Researcher: </label>
    <select name="person">
      <option value="">-----Select Researcher-----</option>
      <?php foreach ($result as $name=>$value){?>
      <option value="<?php echo $value['ResearcherID']?>"><?php echo $value['Name'].' '.$value['Surname'];?></option>
    <?php } ?>
    </select>
</p>

<?php
$sql2 = "SELECT ProjID, Title from project";
$result2 = $conn->query($sql2);
?>
<p>
    <label for="proj">Project: </label>
    <select name="proj">
      <option value="">-----Select Project-----</option>
      <?php foreach ($result2 as $name2=>$value2){?>
      <option value="<?php echo $value2['ProjID']?>"><?php echo $value2['Title'];?></option>
    <?php } ?>
    </select>
</p>

<input type="submit" value="Submit">
</form>

<p>
<a href = "<?php echo "insert.php";?>" ><button class="button button2" >Go Back <br> (Insert Data in another Table)</button></a> <br>
</p>

<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>