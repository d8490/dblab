<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>

<body>


<h1>Insert New Phone Number for Organisation</h1>

<?php
$sql = "SELECT OrgID, Name from organisation";
$result = $conn->query($sql);

?>



<form method="post" action="INS/insert_phone.php">
<p>
    <label for="org">Select Organisation: </label>
    <select name="org">
      <option value="">-----Select Organisation-----</option>
      <?php foreach ($result as $name=>$value){?>
      <option value="<?php echo $value['OrgID']?>"><?php echo $value['Name'];?></option>
    <?php } ?>
    </select>
</p>
<p>
    <label for="num">Phone Number: </label>
    <input type="tel" name="num" id="num">
</p>
<input type="submit" value="Submit">
</form>

<p>
<a href = "<?php echo "insert.php";?>" ><button class="button button2" >Go Back <br> (Insert Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>