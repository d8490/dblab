<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>
<body>

<h1>Insert New Researcher</h1>
<?php
$sql = "SELECT OrgID, Name from organisation";
$result = $conn->query($sql);

?>

<form method="post" action="INS/insert_researcher.php">
<p>
    <label for="name">Name: </label>
    <input type="text" name="name" id="name">
</p>
<p>
    <label for="sname">Surname: </label>
    <input type="text" name="sname" id="sname">
</p>
<p>
    <label for="sex">Gender :</label>
    <input type="radio" id="sex" name="sex" value="M">
    <label for="M">Male</label>
    <input type="radio" id="sex" name="sex" value="F">
    <label for="F">Female</label>
    <input type="radio" id="sex" name="sex" value="O">
    <label for="O">Other</label>

</p>
<p>
    <label for="bd">Birthday: </label>
    <input type="date" name="bd" id="bd">
</p>
<p>
    <label for="org">Organisation: </label>
    <select name="org">
      <option value="">-----Select Organisation-----</option>
      <?php foreach ($result as $name=>$value){?>
      <option value="<?php echo $value['OrgID']?>"><?php echo $value['Name'];?></option>
    <?php } ?>
    </select>
</p>

<p>
    <label for="wd">Started Working at Organisation on: </label>
    <input type="date" name="wd" id="wd">
</p>

<input type="submit" value="Submit">
</form>

<p>
<a href = "<?php echo "insert.php";?>" ><button class="button button2" >Go Back <br> (Insert Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>