<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>
<body>

<h1>Insert New Project</h1>

<?php
$sql = "SELECT ResearcherID, Name, Surname, OrgID from researcher";
$result = $conn->query($sql);

$sql2 = "SELECT OrgID, Name from organisation";
$result2 = $conn->query($sql2);

$sql3 = "SELECT * from executive";
$result3 = $conn->query($sql3);

$sql4 = "SELECT ProgrID, Name from program";
$result4 = $conn->query($sql4);
?>




<form method="post" action="INS/insert_project.php">
<p>
    <label for="title">Title: </label>
    <input type="text" name="title" id="title">
</p>
<p>
    <label for="sum">Summary: </label>
    <textarea id="sum" name="sum" rows="4" cols="50"></textarea>
</p>
<p>
    <label for="budget">Funding Amount: </label>
    <input type="number" name="budget" id="budget">
</p>
<p>
    <label for="st">Starting Date: </label>
    <input type="date" name="st" id="st">
</p>
<p>
    <label for="et">Ending Date: </label>
    <input type="date" name="et" id="et">
</p>
<p>
    <label for="person">Scientific Director: </label>
    <select name="person">
      <option value="">-----Select Researcher-----</option>
      <?php foreach ($result as $name=>$value){?>
      <option value="<?php echo $value['ResearcherID']?>"><?php echo $value['Name'].' '.$value['Surname'];?></option>
    <?php } ?>
    </select>
</p>
<p>
    <label for="evalor">Evaluator: </label>
    <select name="evalor">
      <option value="">-----Select Researcher-----</option>
      <?php foreach ($result as $name=>$value){?>
      <option value="<?php echo $value['ResearcherID']?>"><?php echo $value['Name'].' '.$value['Surname'];?></option>
    <?php } ?>
    </select>
</p>
<p>
    <label for="grade">Evaluation Grade :</label>
    <select id="grade" name="grade">
        <option value="A">A</option>
        <option value="B">B</option>
        <option value="C">C</option>
        <option value="D">D</option>
        <option value="F">F</option>
</select>
</p>
<p>
    <label for="evald">Evaluation Date: </label>
    <input type="date" name="evald" id="evald">
</p>
<p>
    <label for="org">Select Organisation: </label>
    <select name="org">
      <option value="">-----Select Organisation-----</option>
      <?php foreach ($result2 as $name2=>$value2){?>
      <option value="<?php echo $value2['OrgID']?>"><?php echo $value2['Name'];?></option>
    <?php } ?>
    </select>
</p>
<p>
  <label for="exec">Executive:</label>
    <select name="exec">
      <option value="">-----Select Executive-----</option>
      <?php foreach ($result3 as $name3=>$value3){?>
      <option value="<?php echo $value3['ExecID']?>"><?php echo $value3['Name'].' '.$value3['Surname'];?></option>
    <?php } ?>
    </select>
</p>
<p>
    <label for="progr">Program: </label>
    <select name="progr">
      <option value="">-----Select Program-----</option>
      <?php foreach ($result4 as $name4=>$value4){?>
      <option value="<?php echo $value4['ProgrID']?>"><?php echo $value4['Name'];?></option>
    <?php } ?>
    </select>
</p>

<input type="submit" value="Submit">
</form>
<p>
<a href = "<?php echo "insert.php";?>" ><button class="button button2" >Go Back <br> (Insert Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>