<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>
<body>

<h1>Insert New Deliverable</h1>
<?php
$sql = "SELECT ProjID, Title from project";
$result = $conn->query($sql);

?>

<form method="post" action="INS/insert_deliverable.php">
<p>
    <label for="id">Deliverable ID: </label>
    <input type="number" name="id" id="id">
</p>
<p>
    <label for="proj">Project: </label>
    <select name="proj">
      <option value="">-----Select Project-----</option>
      <?php foreach ($result as $name=>$value){?>
      <option value="<?php echo $value['ProjID']?>"><?php echo $value['Title'];?></option>
    <?php } ?>
    </select>
</p>

<p>
    <label for="title">Title: </label>
    <input type="text" name="title" id="title">
</p>
<p>
    <label for="sum">Summary: </label>
    <textarea id="sum" name="sum" rows="4" cols="50"></textarea>
</p>
<p>
    <label for="dt">Delivery Date: </label>
    <input type="date" name="dt" id="dt">
</p>

<input type="submit" value="Submit">
</form>
<p>
<a href = "<?php echo "insert.php";?>" ><button class="button button2" >Go Back <br> (Insert Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>