<!DOCTYPE html>
<?php include '../style.php';?>
<html>
<body>

<h1>Insert New Organisation</h1>


<form method="post" action="INS/insert_organisation.php">
<p>
    <label for="name">Name: </label>
    <input type="text" name="name" id="name">
</p>
<p>
    <label for="abbr">Abbreviation: </label>
    <input type="text" name="abbr" id="abbr">
</p>
<p>
    <label for="street">Street Name: </label>
    <input type="text" name="street" id="street">
</p>
<p>
    <label for="strnum">Street Number: </label>
    <input type="number" name="strnum" id="strnum">
</p>
<p>
    <label for="postcode">Postal Code: </label>
    <input type="number" name="postcode" id="postcode">
</p>
<p>
    <label for="city">City: </label>
    <input type="text" name="city" id="city">
</p>
<p>
    <label for="cat">Type of Organisation :</label>
    <input type="radio" id="cat" name="cat" value="Uni">
    <label for="Uni">University</label>
    <input type="radio" id="cat" name="cat" value="Com">
    <label for="Com">Company</label>
    <input type="radio" id="cat" name="cat" value="Rec">
    <label for="Rec">Research Center</label>

</p>
<p>
    <label for="budget">Organisation Budget: </label>
    <input type="number" name="budget" id="budget">
</p>
<input type="submit" value="Submit">
</form>

<p>
<a href = "<?php echo "insert.php";?>" ><button class="button button2" >Go Back <br> (Insert Data in another Table)</button></a> <br>
</p>

<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>