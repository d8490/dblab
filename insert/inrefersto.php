<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>
<body>

<h1>Project Refering to Scientific Field</h1>
<?php
$sql = "SELECT ProjID, Title from project";
$result = $conn->query($sql);

$sql2 = "SELECT FieldID, Name from scientific_field";
$result2 = $conn->query($sql2);

?>

<form method="post" action="INS/insert_refersto.php">

<p>
    <label for="proj">Project: </label>
    <select name="proj">
      <option value="">-----Select Project-----</option>
      <?php foreach ($result as $name=>$value){?>
      <option value="<?php echo $value['ProjID']?>"><?php echo $value['Title'];?></option>
    <?php } ?>
    </select>
</p>
<p>
    <label for="field">Scientific Field: </label>
    <select name="field">
      <option value="">-----Select Field-----</option>
      <?php foreach ($result2 as $name2=>$value2){?>
      <option value="<?php echo $value2['FieldID']?>"><?php echo $value2['Name'];?></option>
    <?php } ?>
    </select>
</p>

<input type="submit" value="Submit">
</form>

<p>
<a href = "<?php echo "insert.php";?>" ><button class="button button2" >Go Back <br> (Insert Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>