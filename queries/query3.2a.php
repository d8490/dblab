<html>
<body>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<h1>Projects per Researchers </h1>
<?php
$sql = "SELECT * from res_proj;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  echo "<table><tr><th>Researcher ID</th><th>Name</th><th>Surname</th><th>Project</th><th>Title</th></tr>";
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo "<tr><td>" . $row["researcher"]."</td><td>". $row["name"]. "</td><td>" . $row["surname"]. "</td><td>" . $row["proj"]. "</td><td>" . $row["title"]. "</td></tr>";
  }
  echo "</table>";
} else {
  echo "0 results";
}
$conn->close();


?>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>