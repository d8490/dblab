<html>
<body>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>

<h1>Organisations with the same number of projects within two consecutive years</h1>
<?php
$sql = "SELECT * from biyearly;";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table><tr><th>Organisation ID</th><th>Organisation Name</th><th>Years</th><th>Number of Projects per year</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["OrgID"]."</td><td>".$row["Name"]."</td><td>".$row["Year"]."</td><td>".$row["Projects"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>