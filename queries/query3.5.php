<html>
<body>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<h1>Top 3 Pairs of Scientific Fields</h1>
<?php
$sql = "SELECT a.Name AS aName, b.Name AS bName, COUNT(pa.ProjID) as Occurences

FROM project as pa 
INNER JOIN refers_to as ra ON pa.ProjID=ra.ProjID
INNER JOIN scientific_field as a ON ra.FieldID=a.FieldID
INNER JOIN scientific_field as b ON a.FieldID>b.FieldID
INNER JOIN refers_to as rb ON rb.FieldID=b.FieldID
INNER JOIN project as pb ON pb.ProjID=rb.ProjID

WHERE pa.ProjID=pb.ProjID
GROUP BY a.FieldID, b.FieldID

ORDER BY Occurences DESC
limit 3;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table><tr><th>First Scientific Field</th><th>Second Scientific Field</th><th>Occurences of Field Pair</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["aName"]."</td><td>".$row["bName"]."</td><td>".$row["Occurences"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>