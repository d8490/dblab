<html>
<body>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>

<h1>Available Programs</h1>
<?php
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT progrID, Name, StartDate,EndDate,ELIDEK_Department
from program where(datediff(EndDate,now())>0 AND datediff(now(),StartDate)>=0);";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
  echo "<table><tr><th>Program ID</th><th>Program Name</th><th>Start Date</th><th>End Date</th><th>ELIDEK Department</th></tr>";
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo "<tr><td>".$row["progrID"]."</td><td>".$row["Name"]."</td><td>".$row["StartDate"]."</td><td>". $row["EndDate"]."</td><td>".$row["ELIDEK_Department"]."</td></tr>";
  }
  echo "</table>";
} else {
  echo "0 results";
}
$conn->close();


?>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>