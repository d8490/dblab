<html>
<body>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<h1>Young Researchers working on Active Projects</h1>
<h2>(Younger than 40 years old)</h2>
<?php

$sql = "SELECT r.Name, r.Surname, COUNT(p.ProjID) as Active_Projects, (year(now())-year(r.BirthDate)) as Age 
FROM researcher as r use index(researcher_Birthdate)
INNER JOIN works_at as wa ON wa.ResearcherID=r.ResearcherID
INNER JOIN project as p USE INDEX(project_start_date,project_end_date) ON p.ProjID=wa.ProjID
WHERE (DATEDIFF(now(), r.BirthDate)<14600) AND (DATEDIFF(now(), p.StartDate)>0) AND (DATEDIFF(now(), p.EndDate)<0)
group by r.ResearcherID
order by Active_Projects DESC";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
  echo "<table><tr><th>Name</th><th>Surname</th><th>Age</th><th>Active Projects</th></tr>";
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo "<tr><td>" . $row["Name"]."</td><td>". $row["Surname"]. "</td><td>".$row["Age"]."</td><td>" . $row["Active_Projects"]. "</td></tr>";
  }
  echo "</table>";
} else {
  echo "0 results";
}
$conn->close();


?>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>