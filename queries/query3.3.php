<html>
<body>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<h1>Select a Scientific Field</h1>

<?php
$sql = "SELECT * from scientific_field";
$result = $conn->query($sql);

?>
<div class='form-group'>
  <form method="post" action="fieldredirect.php">
  <label for="field_name">Field Name</label>
    <select name="field_name">
      <option value="">-----Select Field-----</option>
      <?php foreach ($result as $name=>$value){?>
      <option value="<?php echo $value['FieldID']?>"><?php echo $value['Name'];?></option>
    <?php } ?>
    </select>
    <input type="submit" value="Submit" name="Submit">
</div>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>