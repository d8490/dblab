<html>
<body>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<h1>Projects</h1>
<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
    $start=$_REQUEST['sdate'];
    $D=$_REQUEST['Duration'];
    $execid=$_REQUEST['executive_name'];


$sql="SELECT p.ProjID as ProjID, p.Title as Title, p.Summary as Summary, p.ExecID as ExecID, p.FundingAmount as FundingAmount,
r.ResearcherID as ResearcherID, r.Name as rName, r.Surname as rSurname
from project as p
INNER JOIN works_at as w ON (w.ProjID = p.ProjID) 
INNER JOIN researcher as r ON (r.ResearcherID = w.ResearcherID) 
where (p.ExecID LIKE '$execid') AND (p.StartDate > '$start') AND ((year(p.EndDate)-year(p.StartDate)) LIKE '$D');
";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table><tr><th>Project</th><th>Title</th><th>Summary</th><th>ExecID</th><th>FundingAmount</th>
    <th>Researcher ID</th><th>Researcher's Name</th><th>Researcher's Surname</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["ProjID"]."</td>
      <td>".$row["Title"]."</td>
      <td>".$row["Summary"]."</td>
      <td>".$row["ExecID"]."</td>
      <td>".$row["FundingAmount"]."</td>
      <td>".$row["ResearcherID"]."</td>
      <td>".$row["rName"]."</td>
      <td>".$row["rSurname"]."</td>
      </tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();
}
?>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>