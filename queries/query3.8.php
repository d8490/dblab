<html>
<body>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>

<h1>Researchers that work on Projects With no Deliverables</h1>
<h2>(at least 5 projects with no deliverables)</h2>
<?php
$sql = "SELECT r.Name, r.Surname, COUNT(deliver.ProjID) as Projects_Without_Deliverables
FROM researcher as r
INNER JOIN works_at as wa USE INDEX(work_ResearcherID) ON r.ResearcherID=wa.ResearcherID
INNER JOIN	(SELECT p.ProjID, COUNT(d.DeliverID) as delcnt
			FROM deliverable as d RIGHT JOIN  project as p ON d.ProjID=p.ProjID
			GROUP BY p.ProjID) as deliver
ON wa.ProjID=deliver.ProjID
WHERE deliver.delcnt=0 
GROUP BY r.ResearcherID
HAVING Projects_Without_Deliverables>4;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table><tr><th>Name</th><th>Surname</th><th>Projects_Without_Deliverables</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["Name"]."</td><td>".$row["Surname"]."</td><td>".$row["Projects_Without_Deliverables"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>