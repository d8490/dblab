<html>
<body>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>

<h1>Scientific Fields of Projects</h1>
<?php
$sql = "select * from proj_field;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table><tr><th>Project ID</th><th>Project Name</th><th>Scientific Field Name</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["proj"]."</td><td>".$row["title"]."</td><td>".$row["scientificfield"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>
