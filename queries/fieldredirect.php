<html>
<body>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<h1>Projects and Researchers</h1>
<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
    $id=$_REQUEST['field_name'];


$sql="SELECT f.proj, f.title, r.researcher, r.name, r.surname from field_proj f
INNER JOIN project p on (p.ProjID=f.proj)
INNER JOIN res_proj r on (r.proj=f.proj)
where datediff(now(),p.EndDate)<0 AND datediff(now(),p.StartDate)>0 AND (scientificfield LIKE '$id');
";

//$sql = "SELECT a.Name AS aName, COUNT(pa.ProjID) as Occurences
//FROM project as pa 
//INNER JOIN refers_to as ra ON pa.ProjID=ra.ProjID
//INNER JOIN scientific_field as a ON ra.FieldID=a.FieldID
//WHERE a.FieldID LIKE '$id' 
//GROUP BY a.FieldID
//ORDER BY Occurences DESC;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table><tr><th>Project</th><th>Title</th><th>ResearcherID</th><th>Name</th><th>Surname</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["proj"]."</td><td>".$row["title"]."</td><td>".$row["researcher"]."</td><td>".$row["name"]."</td><td>".$row["surname"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();
}
?>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>