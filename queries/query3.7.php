<html>
<body>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<h1>Top 5 Executives that have funded a Specific Company</h1>
<?php
$sql = "SELECT e.ExecID, e.Name AS eName, e.Surname, o.Name, SUM(p.FundingAmount) AS sum
	
FROM executive as e
INNER JOIN project as p USE INDEX(project_funding_amount) ON p.ExecID=e.ExecID
INNER JOIN company as c ON c.CompID=p.OrgID
INNER JOIN organisation as o ON o.OrgID=c.CompID
GROUP BY p.ExecID, o.OrgID
ORDER BY sum DESC
limit 5;";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    echo "<table><tr><th>Executive ID</th><th>Name</th><th>Surname</th><th>Company Name</th><th>Sum of Funding</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["ExecID"]."</td><td>".$row["eName"]."</td><td>".$row["Surname"]."</td><td>".$row["Name"]."</td><td>".$row["sum"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>