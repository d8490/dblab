<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>

<body>


<h1>Delete an Organisation Phone</h1>

<?php
$sql = "SELECT p.OrgID as OrgID, p.PhoneNumber as Phone, o.Name as Name
 from phone as p 
INNER JOIN organisation as o ON (p.OrgID=o.OrgID)
";
$result = $conn->query($sql);
?>

<form method="post" action="DEL/delete_phone.php">
<p>
    <label for="phone">Select Phone Number: </label>
    <input type="text" name="phone" id="phone">
</p>
<input type="submit" value="Delete Tuple">
</form>
<br>


<?php
if ($result->num_rows > 0) {
    echo "<table><tr><th>Organisation Id</th><th>Name</th><th>Phone Number</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["OrgID"]."</td>
      <td>".$row["Name"]."</td>
      <td>".$row["Phone"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<p>
<a href = "<?php echo "update.php";?>" ><button class="button button2" >Go Back <br> (Update Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>