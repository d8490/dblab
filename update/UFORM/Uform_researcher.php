<!DOCTYPE html>
<?php include '../../config.php';?>
<?php include '../../connection.php';?>
<?php include '../../style.php';?>
<html>
<body>

<h1>Update or Delete Researcher</h1>
<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
    $id=$_REQUEST['id'];
}

$sql = "SELECT * from researcher WHERE ResearcherID like '$id'";
$result = $conn->query($sql);
$sql2 = "SELECT OrgID, Name from organisation";
$result2 = $conn->query($sql2);

if ($result->num_rows > 0) { 
    foreach ($result as $name=>$value){ ?>

<form method="post" action="../UPD/update_researcher.php">
<p>

    <label for="id">Researcher ID: </label>
    <input type="number" name="id" id="id"  value="<?php echo $id ?>" readonly>
</p>
<p>
    <label for="name">Name: </label>
    <input type="text" name="name" id="name" value="<?php echo $value['Name'] ?>">
</p>
<p>
    <label for="sname">Surname: </label>
    <input type="text" name="sname" id="sname" value="<?php echo $value['Surname'] ?>">
</p>
<p>
    <label for="sex">Gender :</label>
    <input type="radio" id="sex" name="sex" value="M">
    <label for="M">Male</label>
    <input type="radio" id="sex" name="sex" value="F">
    <label for="F">Female</label>
    <input type="radio" id="sex" name="sex" value="O" checked>
    <label for="O">Other</label>

</p>
<p>
    <label for="bd">Birthday: </label>
    <input type="date" name="bd" id="bd" value="<?php echo $value['BirthDate'] ?>">
</p>
<p>
    <label for="org">Organisation: </label>
    <select name="org">
      <option value="<?php echo $value['OrgID'] ?>">Current Organisation</option>
      <?php foreach ($result2 as $name2=>$value2){?>
      <option value="<?php echo $value2['OrgID']?>"><?php echo $value2['Name'];?></option>
    <?php } ?>
    </select>
</p>

<p>
    <label for="wd">Started Working at Organisation on: </label>
    <input type="date" name="wd" id="wd" value="<?php echo $value['Work_startdate'] ?>">
</p>

<input type="submit" value="Update">
</form>
<p>
  <?php
    }
  } else {
    echo "Invalid ID";
  }
  $conn->close();
?>
<p>
<a href = "<?php echo "../updresearcher.php";?>" ><button class="button button1" >Update Another Tuple...</button></a> <br>
</p>
<p>
<a href = "<?php echo "../update.php";?>" ><button class="button button2" >Go Back <br> (Update Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>