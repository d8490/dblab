<!DOCTYPE html>
<?php include '../../config.php';?>
<?php include '../../connection.php';?>
<?php include '../../style.php';?>
<html>
<body>

<h1>Update or Delete Organisation</h1>


<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
    $id=$_REQUEST['id'];
}

$sql = "SELECT * from organisation WHERE OrgID like '$id'";
$result = $conn->query($sql);

if ($result->num_rows > 0) { 
    foreach ($result as $name=>$value){ ?>

<form method="post" action="../UPD/update_organisation.php">
<p>
    <label for="id">Organisation ID: </label>
    <input type="number" id="id" name="id" value="<?php echo $id ?>" readonly>
</p>
<p>
    <label for="name">Name: </label>
    <input type="text" name="name" id="name" value="<?php echo $value['Name']?>">
</p>
<p>
    <label for="abbr">Abbreviation: </label>
    <input type="text" name="abbr" id="abbr" value="<?php echo $value['Abbreviation']?>">
</p>
<p>
    <label for="street">Street Name: </label>
    <input type="text" name="street" id="street" value="<?php echo $value['Street']?>">
</p>
<p>
    <label for="strnum">Street Number: </label>
    <input type="number" name="strnum" id="strnum" value="<?php echo $value['StrNumber']?>">
</p>
<p>
    <label for="postcode">Postal Code: </label>
    <input type="number" name="postcode" id="postcode" value="<?php echo $value['PostalCode']?>">
</p>
<p>
    <label for="city">City: </label>
    <input type="text" name="city" id="city" value="<?php echo $value['City']?>">
</p>

<p>
    <label for="budget">Organisation Budget: </label>
    <input type="number" name="budget" id="budget" value="<?php echo $value['Budget']?>">
</p>

<input type="submit" value="Update">
</form>
<p>


  <?php
    }
  } else {
    echo "Invalid ID";
  }
  $conn->close();
?>
<p>
<a href = "<?php echo "../updorg.php";?>" ><button class="button button1" >Update Another Tuple...</button></a> <br>
</p>
<p>
<a href = "<?php echo "../update.php";?>" ><button class="button button2" >Go Back <br> (Update Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>

</body>
</html>