<!DOCTYPE html>
<?php include '../../config.php';?>
<?php include '../../connection.php';?>
<?php include '../../style.php';?>
<html>
<body>

<h1>Update or Delete Project</h1>

<?php
if($_SERVER["REQUEST_METHOD"]=="POST"){
    $id=$_REQUEST['id'];
}

$sql = "SELECT * from project WHERE ProjID like '$id'";
$result = $conn->query($sql);

$sql5 = "SELECT ResearcherID, Name, Surname from researcher";
$result5 = $conn->query($sql5);

$sql2 = "SELECT OrgID, Name from organisation";
$result2 = $conn->query($sql2);

$sql3 = "SELECT * from executive";
$result3 = $conn->query($sql3);

$sql4 = "SELECT ProgrID, Name from program";
$result4 = $conn->query($sql4);



if ($result->num_rows > 0) { 
    foreach ($result as $name=>$value){ ?>

<form method="post" action="../UPD/update_project.php">
<p>
    <label for="id">Project ID: </label>
    <input type="number" name="id" id="id"  value="<?php echo $id ?>" readonly>
</p>
<p>
    <label for="title">Title: </label>
    <input type="text" name="title" id="title" value="<?php echo $value['Title'] ?>">
</p>
<p>
    <label for="sum">Summary: </label>
    <textarea id="sum" name="sum" rows="4" cols="50" ><?php echo $value['Summary'] ?></textarea>
</p>
<p>
    <label for="budget">Funding Amount: </label>
    <input type="number" name="budget" id="budget" value="<?php echo $value['FundingAmount']?>">
</p>
<p>
    <label for="sd">Starting Date: </label>
    <input type="date" name="sd" id="sd" value="<?php echo $value['StartDate']?>">
</p>
<p>
    <label for="ed">Ending Date: </label>
    <input type="date" name="ed" id="ed" value="<?php echo $value['EndDate']?>">
</p>
<p>
    <label for="person">Scientific Director: </label>
    <select name="person">
      <option selected value="<?php echo $value['ScientificDirectorID']?>">Current Scientific Director</option>
      <?php foreach ($result5 as $name5=>$value5){?>
      <option value="<?php echo $value5['ResearcherID']?>"><?php echo $value5['Name'].' '.$value5['Surname'];?></option>
    <?php } ?>
    </select>
</p>
<p>
    <label for="evalor">Evaluator: </label>
    <select name="evalor">
      <option selected value="<?php echo $value['EvaluatorID']?>">Current Evaluator</option>
      <?php foreach ($result5 as $name5=>$value5){?>
      <option value="<?php echo $value5['ResearcherID']?>"><?php echo $value5['Name'].' '.$value5['Surname'];?></option>
    <?php } ?>
    </select>
</p>
<p>
    <label for="grade">Evaluation Grade :</label>
    <select id="grade" name="grade">
        <option value="A" selected>A</option>
        <option value="B">B</option>
        <option value="C">C</option>
        <option value="D">D</option>
        <option value="F">F</option>
</select>
</p>
<p>
    <label for="evald">Evaluation Date: </label>
    <input type="date" name="evald" id="evald" value="<?php echo $value['EvalDate']?>">
</p>
<p>
    <label for="org">Select Organisation: </label>
    <select name="org">
      <option value="<?php echo $value['OrgID']?>">Current Organisation</option>
      <?php foreach ($result2 as $name2=>$value2){?>
      <option value="<?php echo $value2['OrgID']?>"><?php echo $value2['Name'];?></option>
    <?php } ?>
    </select>
</p>
<p>
  <label for="exec">Executive:</label>
    <select name="exec">
      <option value="<?php echo $value['ExecID']?>">Current Executive</option>
      <?php foreach ($result3 as $name3=>$value3){?>
      <option value="<?php echo $value3['ExecID']?>"><?php echo $value3['Name'].' '.$value3['Surname'];?></option>
    <?php } ?>
    </select>
</p>
<p>
    <label for="progr">Program: </label>
    <select name="progr">
      <option value="<?php echo $value['ProgrID']?>">Current Program</option>
      <?php foreach ($result4 as $name4=>$value4){?>
      <option value="<?php echo $value4['ProgrID']?>"><?php echo $value4['Name'];?></option>
    <?php } ?>
    </select>
</p>
<input type="submit" value="Update">
</form>

<?php
    }
  } else {
    echo "Invalid ID";
  }
  $conn->close();
?>
<p>
<a href = "<?php echo "../updproj.php";?>" ><button class="button button1" >Update Another Tuple...</button></a> <br>
</p>
<p>
<a href = "<?php echo "../update.php";?>" ><button class="button button2" >Go Back <br> (Update Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>

</body>
</html>