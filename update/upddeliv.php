<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>

<body>


<h1>Update or Delete a Deliverable</h1>

<?php
$sql = "SELECT * from deliverable";
$result = $conn->query($sql);
?>

<form method="post" action="UFORM/Uform_deliverable.php">
<p>
    <label for="id">Select Deliverable ID: </label>
    <input type="number" name="id" id="id">
</p>

<input type="submit" value="Update">
<input type="submit" value="Delete Tuple" formaction="DEL/delete_deliverable.php">
<br>
</form>


<?php
if ($result->num_rows > 0) {
    echo "<table><tr><th>DeliverID</th><th>ProjectID</th><th>Title</th><th>Summary</th><th>DeliveryDate</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["DeliverID"]."</td><td>".$row["ProjID"]."</td><td>".$row["Title"]."</td><td>".$row["Summary"]."</td><td>".$row["DeliveryDate"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>

<p>
<a href = "<?php echo "update.php";?>" ><button class="button button2" >Go Back <br> (Update Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>