<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>

<body>


<h1>Delete a 'Project refers to Scientific Field' relationship </h1>


<?php
$sql = "SELECT * from refers_to as rt 
INNER JOIN project as p ON (p.ProjID=rt.ProjID)
INNER JOIN scientific_field as s ON (s.FieldID=rt.FieldID)
ORDER BY rt.ProjID;";
$result = $conn->query($sql);
?>

<form method="post" action="DEL/delete_refersto.php">
<p>
    <label for="pid">Select Project ID: </label>
    <input type="number" name="pid" id="pid">
</p>
<p>
    <label for="fid">Select Field ID: </label>
    <input type="number" name="fid" id="fid">
</p>
<input type="submit" value="Delete Tuple">
</form>
<br>

<?php
if ($result->num_rows > 0) {
    echo "<table><tr><th>Project ID</th>
    <th>Title</th>
    <th>Summary</th>
    <th>Field ID</th>
    <th>Name</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["ProjID"]."</td>
      <td>".$row["Title"]."</td>
      <td>".$row["Summary"]."</td>
      <td>".$row["FieldID"]."</td>
      <td>".$row["Name"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<p>
<a href = "<?php echo "update.php";?>" ><button class="button button2" >Go Back <br> (Update Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>