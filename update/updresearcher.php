<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>

<body>


<h1>Update or Delete a Researcher</h1>

<?php
$sql = "SELECT * from researcher;";
$result = $conn->query($sql);
?>

<form method="post" action="UFORM/Uform_researcher.php">
<p>
    <label for="id">Select Researcher ID: </label>
    <input type="number" name="id" id="id">
</p>
<input type="submit" value="Update">
<input type="submit" value="Delete Tuple" formaction="DEL/delete_researcher.php">
<br>
</form>


<?php
if ($result->num_rows > 0) {
    echo "<table><tr><th>Researcher ID</th>
    <th>Name</th>
    <th>Surname</th>
    <th>Sex</th>
    <th>Birth Date</th>
    <th>Organisation ID</th>
    <th>Work Start Date</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["ResearcherID"]."</td>
      <td>".$row["Name"]."</td>
      <td>".$row["Surname"]."</td>
      <td>".$row["Sex"]."</td>
      <td>".$row["BirthDate"]."</td>
      <td>".$row["OrgID"]."</td>
      <td>".$row["Work_startdate"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<p>
<a href = "<?php echo "update.php";?>" ><button class="button button2" >Go Back <br> (Update Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>