<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>

<body>


<h1>Delete a "Researcher Working on Project" relationship </h1>

<?php
$sql = "SELECT * from works_at as wa 
INNER JOIN project as p ON (p.ProjID=wa.ProjID)
INNER JOIN researcher as r ON (r.ResearcherID=wa.ResearcherID);";
$result = $conn->query($sql);
?>

<form method="post" action="DEL/delete_worksat.php">
<p>
    <label for="rid">Select Researcher ID: </label>
    <input type="number" name="rid" id="rid">
</p>
<p>
    <label for="pid">Select Project ID: </label>
    <input type="number" name="pid" id="pid">
</p>
<input type="submit" value="Delete Tuple">
</form>
<br>


<?php
if ($result->num_rows > 0) {
    echo "<table><tr><th>Researcher ID</th><th>Name</th><th>Surname</th><th>Start Date of Work</th><th>Project ID</th><th>Title</th><th>Summary</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["ResearcherID"]."</td>
      <td>".$row["Name"]."</td>
      <td>".$row["Surname"]."</td>
      <td>".$row["StartDate"]."</td>
      <td>".$row["ProjID"]."</td>
      <td>".$row["Title"]."</td>
      <td>".$row["Summary"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<p>
<a href = "<?php echo "update.php";?>" ><button class="button button2" >Go Back <br> (Update Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>