<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>

<body>


<h1>Update or Delete a Project</h1>

<?php
$sql = "SELECT * from project";
$result = $conn->query($sql);
?>

<form method="post" action="UFORM/Uform_project.php">
<p>
    <label for="id">Select Project ID: </label>
    <input type="number" name="id" id="id">
</p>
<input type="submit" value="Update">
<input type="submit" value="Delete Tuple" formaction="DEL/delete_project.php">
<br>
</form>


<?php
if ($result->num_rows > 0) {
    echo "<table><tr><th>Project ID</th><th>Title</th><th>Summary</th><th>Funding Amount</th><th>Start Date</th><th>End Date</th><th>Scientific Director ID</th><th>Evaluator ID</th><th>Evaluation Grade</th><th>Evaluation Date</th><th>Org ID</th><th>Exec ID</th><th>Progr ID</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["ProjID"]."</td><td>".$row["Title"]."</td><td>".$row["Summary"]."</td><td>".$row["FundingAmount"]."</td><td>".$row["StartDate"]."</td><td>".$row["EndDate"]."</td><td>".$row["ScientificDirectorID"]."</td><td>".$row["EvaluatorID"]."</td><td>".$row["EvalGrade"]."</td><td>".$row["EvalDate"]."</td><td>".$row["OrgID"]."</td><td>".$row["ExecID"]."</td><td>".$row["ProgrID"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<p>
<a href = "<?php echo "update.php";?>" ><button class="button button2" >Go Back <br> (Update Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>