<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>

<body>


<h1>Update or Delete an Organisation</h1>

<?php
$sql = "SELECT * from organisation";
$result = $conn->query($sql);
?>

<form method="post" action="UFORM/Uform_organisation.php">
<p>
    <label for="id">Select Organisation ID: </label>
    <input type="number" name="id" id="id">
</p>
<input type="submit" value="Update">
<input type="submit" value="Delete Tuple" formaction="DEL/delete_organisation.php">
<br>
</form>


<?php
if ($result->num_rows > 0) {
    echo "<table><tr><th>Organisation ID</th><th>Abbreviation</th><th>Name</th><th>Street</th><th>Street Number</th><th>Postal Code</th><th>City</th><th>Category</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["OrgID"]."</td><td>".$row["Abbreviation"]."</td><td>".$row["Name"]."</td><td>".$row["Street"]."</td><td>".$row["StrNumber"]."</td><td>".$row["PostalCode"]."</td><td>".$row["City"]."</td><td>".$row["Category"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<p>
<a href = "<?php echo "update.php";?>" ><button class="button button2" >Go Back <br> (Update Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>