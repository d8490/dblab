<!DOCTYPE html>
<?php include '../config.php';?>
<?php include '../connection.php';?>
<?php include '../style.php';?>
<html>

<body>


<h1>Update or Delete a Program</h1>

<?php
$sql = "SELECT * from program";
$result = $conn->query($sql);
?>

<form method="post" action="UFORM/Uform_program.php">
<p>
    <label for="id">Select Program ID: </label>
    <input type="number" name="id" id="id">
</p>
<input type="submit" value="Update">
<input type="submit" value="Delete Tuple" formaction="DEL/delete_program.php">
<br>
</form>


<?php
if ($result->num_rows > 0) {
    echo "<table><tr><th>Program ID</th><th>Name</th><th>Start Date</th><th>End Date</th><th>ELIDEK Department</th></tr>";
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>".$row["ProgrID"]."</td><td>".$row["Name"]."</td><td>".$row["StartDate"]."</td><td>".$row["EndDate"]."</td><td>".$row["ELIDEK_Department"]."</td></tr>";
    }
    echo "</table>";
  } else {
    echo "0 results";
  }
  $conn->close();

?>
<p>
<a href = "<?php echo "update.php";?>" ><button class="button button2" >Go Back <br> (Update Data in another Table)</button></a> <br>
</p>
<a href = "<?php echo "../index.php";?>" ><button class="button button3" ><i class="fa fa-home"></i> Home</button></a>
</body>
</html>